/*
 *
 *  Wireless daemon for Linux
 *
 *  Copyright (C) 2013-2019  Intel Corporation. All rights reserved.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <errno.h>
#include <linux/if_ether.h>

#include <ell/ell.h>

#include "src/iwd.h"
#include "src/module.h"
#include "src/util.h"
#include "src/wiphy.h"
#include "src/scan.h"
#include "src/netdev.h"
#include "src/station.h"

struct device {
    uint32_t index;

    struct wiphy *wiphy;
    struct netdev *netdev;

    bool powered:1;             /* Current IFUP state */
};

static uint32_t netdev_watch;

static struct device *device_create(struct wiphy *wiphy, struct netdev *netdev)
{
    struct device *device;
    uint32_t ifindex = netdev_get_ifindex(netdev);

    device = l_new(struct device, 1);
    device->index = ifindex;
    device->wiphy = wiphy;
    device->netdev = netdev;
    device->powered = netdev_get_is_up(netdev);

    return device;
}

static void device_netdev_notify(struct netdev *netdev, enum netdev_watch_event event, void *user_data)
{
    struct device *device = NULL;

    switch (event) {
        case NETDEV_WATCH_EVENT_NEW:
            if (L_WARN_ON(device))
                break;

            if (netdev_get_iftype(netdev) == NETDEV_IFTYPE_P2P_CLIENT ||
                netdev_get_iftype(netdev) == NETDEV_IFTYPE_P2P_GO)
                return;

            device_create(netdev_get_wiphy(netdev), netdev);
            break;
        case NETDEV_WATCH_EVENT_DEL:
            break;
        case NETDEV_WATCH_EVENT_UP:
            device->powered = true;
            break;
        case NETDEV_WATCH_EVENT_DOWN:
            device->powered = false;
            break;
        case NETDEV_WATCH_EVENT_NAME_CHANGE:
            break;
        case NETDEV_WATCH_EVENT_ADDRESS_CHANGE:
            break;
        default:
            break;
    }
}

static int device_init(void)
{
    netdev_watch = netdev_watch_add(device_netdev_notify, NULL, NULL);

    return 0;
}

static void device_exit(void)
{
    netdev_watch_remove(netdev_watch);
}

IWD_MODULE(device, device_init, device_exit)
