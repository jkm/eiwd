/*
 *
 *  Wireless daemon for Linux
 *
 *  Copyright (C) 2019  Intel Corporation. All rights reserved.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <errno.h>
#include <arpa/inet.h>
#include <net/if_arp.h>
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <linux/rtnetlink.h>
#include <stdlib.h>

#include <ell/ell.h>

#include "src/iwd.h"
#include "src/module.h"
#include "src/netdev.h"
#include "src/station.h"
#include "src/common.h"
#include "src/network.h"
#include "src/netconfig.h"

struct netconfig {
    uint32_t ifindex;
    struct l_queue *ifaddr_list;
    uint8_t rtm_protocol;

    const struct l_settings *active_settings;

    netconfig_notify_func_t notify;
    void *user_data;
};

struct netconfig_ifaddr {
    uint8_t family;
    uint8_t prefix_len;
    char *ip;
    char *broadcast;
};

static struct l_netlink *rtnl;
static struct l_queue *netconfig_list;

/*
 * Routing priority offset, configurable in main.conf. The route with lower
 * priority offset is preferred.
 */
static uint32_t ROUTE_PRIORITY_OFFSET;

static void do_debug(const char *str, void *user_data)
{
    const char *prefix = user_data;

    l_info("%s%s", prefix, str);
}

static void netconfig_ifaddr_destroy(void *data)
{
    struct netconfig_ifaddr *ifaddr = data;

    l_free(ifaddr->ip);
    l_free(ifaddr->broadcast);

    l_free(ifaddr);
}

static void netconfig_free(void *data)
{
    struct netconfig *netconfig = data;

    l_queue_destroy(netconfig->ifaddr_list, netconfig_ifaddr_destroy);

    l_free(netconfig);
}

static struct netconfig *netconfig_find(uint32_t ifindex)
{
    const struct l_queue_entry *entry;

    for (entry = l_queue_get_entries(netconfig_list); entry; entry = entry->next) {
        struct netconfig *netconfig = entry->data;

        if (netconfig->ifindex != ifindex)
            continue;

        return netconfig;
    }

    return NULL;
}

static struct netconfig_ifaddr *netconfig_ipv4_get_ifaddr(struct netconfig *netconfig, uint8_t proto)
{
    struct netconfig_ifaddr *ifaddr;
    struct in_addr in_addr;
    char *netmask;
    char *ip;

    ip = l_settings_get_string(netconfig->active_settings, "IP", "Address");
    if (!ip)
        return NULL;

    ifaddr = l_new(struct netconfig_ifaddr, 1);
    ifaddr->ip = ip;

    netmask = l_settings_get_string(netconfig->active_settings, "IP", "Netmask");

    if (netmask && inet_pton(AF_INET, netmask, &in_addr) > 0)
        ifaddr->prefix_len = __builtin_popcountl(L_BE32_TO_CPU(in_addr.s_addr));
    else
        ifaddr->prefix_len = 24;

    l_free(netmask);

    ifaddr->broadcast = l_settings_get_string(netconfig->active_settings, "IP", "Broadcast");

    ifaddr->family = AF_INET;

    return ifaddr;
}

static char *netconfig_ipv4_get_gateway(struct netconfig *netconfig)
{
    char *gateway;

    gateway = l_settings_get_string(netconfig->active_settings, "IP", "Gateway");
    if (!gateway)
        return NULL;

    return gateway;
}

static bool netconfig_ifaddr_match(const void *a, const void *b)
{
    const struct netconfig_ifaddr *entry = a;
    const struct netconfig_ifaddr *query = b;

    if (entry->family != query->family)
        return false;

    if (entry->prefix_len != query->prefix_len)
        return false;

    if (strcmp(entry->ip, query->ip))
        return false;

    return true;
}

static struct netconfig_ifaddr *netconfig_ifaddr_find(const struct netconfig *netconfig,
                                                      uint8_t family, uint8_t prefix_len, const char *ip)
{
    const struct l_queue_entry *entry;

    for (entry = l_queue_get_entries(netconfig->ifaddr_list); entry; entry = entry->next) {
        struct netconfig_ifaddr *ifaddr = entry->data;

        if (ifaddr->family != family)
            continue;

        if (ifaddr->prefix_len != prefix_len)
            continue;

        if (strcmp(ifaddr->ip, ip))
            continue;

        return ifaddr;
    }

    return NULL;
}

static void netconfig_ifaddr_added(struct netconfig *netconfig, const struct ifaddrmsg *ifa, uint32_t len)
{
    struct netconfig_ifaddr *ifaddr;
    char *label;

    ifaddr = l_new(struct netconfig_ifaddr, 1);
    ifaddr->family = ifa->ifa_family;
    ifaddr->prefix_len = ifa->ifa_prefixlen;

    l_rtnl_ifaddr4_extract(ifa, len, &label, &ifaddr->ip, &ifaddr->broadcast);

    l_debug("%s: ifaddr %s/%u broadcast %s", label, ifaddr->ip, ifaddr->prefix_len, ifaddr->broadcast);
    l_free(label);

    l_queue_push_tail(netconfig->ifaddr_list, ifaddr);
}

static void netconfig_ifaddr_deleted(struct netconfig *netconfig, const struct ifaddrmsg *ifa, uint32_t len)
{
    struct netconfig_ifaddr *ifaddr;
    struct netconfig_ifaddr query;

    l_rtnl_ifaddr4_extract(ifa, len, NULL, &query.ip, NULL);

    query.family = ifa->ifa_family;
    query.prefix_len = ifa->ifa_prefixlen;

    ifaddr = l_queue_remove_if(netconfig->ifaddr_list, netconfig_ifaddr_match, &query);
    l_free(query.ip);

    if (!ifaddr)
        return;

    l_debug("ifaddr %s/%u", ifaddr->ip, ifaddr->prefix_len);

    netconfig_ifaddr_destroy(ifaddr);
}

static void netconfig_ifaddr_notify(uint16_t type, const void *data, uint32_t len, void *user_data)
{
    const struct ifaddrmsg *ifa = data;
    struct netconfig *netconfig;
    uint32_t bytes;

    netconfig = netconfig_find(ifa->ifa_index);
    if (!netconfig)
        /* Ignore the interfaces which aren't managed by iwd. */
        return;

    bytes = len - NLMSG_ALIGN(sizeof(struct ifaddrmsg));

    switch (type) {
        case RTM_NEWADDR:
            netconfig_ifaddr_added(netconfig, ifa, bytes);
            break;
        case RTM_DELADDR:
            netconfig_ifaddr_deleted(netconfig, ifa, bytes);
            break;
    }
}

static void netconfig_ifaddr_cmd_cb(int error, uint16_t type, const void *data, uint32_t len, void *user_data)
{
    if (error) {
        l_error("netconfig: ifaddr command failure. " "Error %d: %s", error, strerror(-error));
        return;
    }

    if (type != RTM_NEWADDR)
        return;

    netconfig_ifaddr_notify(type, data, len, user_data);
}

static void netconfig_route_add_cmd_cb(int error, uint16_t type, const void *data, uint32_t len, void *user_data)
{
    struct netconfig *netconfig = user_data;

    if (error) {
        l_error("netconfig: Failed to add route. Error %d: %s", error, strerror(-error));
        return;
    }

    if (!netconfig->notify)
        return;

    netconfig->notify(NETCONFIG_EVENT_CONNECTED, netconfig->user_data);
    netconfig->notify = NULL;
}

static bool netconfig_ipv4_routes_install(struct netconfig *netconfig, struct netconfig_ifaddr *ifaddr)
{
    L_AUTO_FREE_VAR(char *, gateway) = NULL;
    struct in_addr in_addr;
    char *network;

    if (inet_pton(AF_INET, ifaddr->ip, &in_addr) < 1)
        return false;

    in_addr.s_addr = in_addr.s_addr & htonl(0xFFFFFFFFLU << (32 - ifaddr->prefix_len));

    network = inet_ntoa(in_addr);
    if (!network)
        return false;

    if (!l_rtnl_route4_add_connected(rtnl, netconfig->ifindex,
                                     ifaddr->prefix_len, network,
                                     ifaddr->ip,
                                     netconfig->rtm_protocol, netconfig_route_add_cmd_cb, netconfig, NULL)) {
        l_error("netconfig: Failed to add subnet route.");

        return false;
    }

    gateway = netconfig_ipv4_get_gateway(netconfig);
    if (!gateway) {
        l_error("netconfig: Failed to obtain gateway from %s.",
                netconfig->rtm_protocol == RTPROT_STATIC ? "setting file" : "DHCPv4 lease");

        return false;
    }

    if (!l_rtnl_route4_add_gateway(rtnl, netconfig->ifindex, gateway,
                                   ifaddr->ip,
                                   ROUTE_PRIORITY_OFFSET,
                                   netconfig->rtm_protocol, netconfig_route_add_cmd_cb, netconfig, NULL)) {
        l_error("netconfig: Failed to add route for: %s gateway.", gateway);

        return false;
    }

    return true;
}

static void netconfig_ipv4_ifaddr_add_cmd_cb(int error, uint16_t type, const void *data, uint32_t len, void *user_data)
{
    struct netconfig *netconfig = user_data;
    struct netconfig_ifaddr *ifaddr;

    if (error && error != -EEXIST) {
        l_error("netconfig: Failed to add IP address. " "Error %d: %s", error, strerror(-error));
        return;
    }

    ifaddr = netconfig_ipv4_get_ifaddr(netconfig, netconfig->rtm_protocol);
    if (!ifaddr) {
        l_error("netconfig: Failed to obtain IP address from %s.",
                netconfig->rtm_protocol == RTPROT_STATIC ? "setting file" : "DHCPv4 lease");
        return;
    }

    if (!netconfig_ipv4_routes_install(netconfig, ifaddr)) {
        l_error("netconfig: Failed to install IPv4 routes.");

        goto done;
    }

 done:
    netconfig_ifaddr_destroy(ifaddr);
}

static void netconfig_install_address(struct netconfig *netconfig, struct netconfig_ifaddr *ifaddr)
{
    if (netconfig_ifaddr_find(netconfig, ifaddr->family, ifaddr->prefix_len, ifaddr->ip))
        return;

    if (l_rtnl_ifaddr4_add(rtnl, netconfig->ifindex,
                           ifaddr->prefix_len, ifaddr->ip,
                           ifaddr->broadcast, netconfig_ipv4_ifaddr_add_cmd_cb, netconfig, NULL))
        return;

    l_error("netconfig: Failed to set IP %s/%u.", ifaddr->ip, ifaddr->prefix_len);
}

static void netconfig_ifaddr_del_cmd_cb(int error, uint16_t type, const void *data, uint32_t len, void *user_data)
{
    if (error == -ENODEV)
        /* The device is unplugged, we are done. */
        return;

    if (!error)
        /*
         * The kernel removes all of the routes associated with the
         * deleted IP on its own. There is no need to explicitly remove
         * them.
         */
        return;

    l_error("netconfig: Failed to delete IP address. " "Error %d: %s", error, strerror(-error));
}

static void netconfig_uninstall_address(struct netconfig *netconfig, struct netconfig_ifaddr *ifaddr)
{
    if (!netconfig_ifaddr_find(netconfig, ifaddr->family, ifaddr->prefix_len, ifaddr->ip))
        return;

    if (l_rtnl_ifaddr4_delete(rtnl, netconfig->ifindex,
                              ifaddr->prefix_len, ifaddr->ip,
                              ifaddr->broadcast, netconfig_ifaddr_del_cmd_cb, netconfig, NULL))
        return;

    l_error("netconfig: Failed to delete IP %s/%u.", ifaddr->ip, ifaddr->prefix_len);
}

static void netconfig_ipv4_select_and_install(struct netconfig *netconfig)
{
    struct netconfig_ifaddr *ifaddr;

    ifaddr = netconfig_ipv4_get_ifaddr(netconfig, RTPROT_STATIC);
    if (ifaddr) {
        netconfig->rtm_protocol = RTPROT_STATIC;
        netconfig_install_address(netconfig, ifaddr);
        netconfig_ifaddr_destroy(ifaddr);

        return;
    }

    l_error("netconfig: Failed get ifaddr for interface %u", netconfig->ifindex);
}

static void netconfig_ipv4_select_and_uninstall(struct netconfig *netconfig)
{
    struct netconfig_ifaddr *ifaddr;

    ifaddr = netconfig_ipv4_get_ifaddr(netconfig, netconfig->rtm_protocol);
    if (ifaddr) {
        netconfig_uninstall_address(netconfig, ifaddr);
        netconfig_ifaddr_destroy(ifaddr);
    }
}

bool netconfig_configure(struct netconfig *netconfig,
                         const struct l_settings *active_settings,
                         const uint8_t * mac_address, netconfig_notify_func_t notify, void *user_data)
{
    netconfig->active_settings = active_settings;
    netconfig->notify = notify;
    netconfig->user_data = user_data;

    netconfig_ipv4_select_and_install(netconfig);

    return true;
}

bool netconfig_reconfigure(struct netconfig *netconfig)
{
    return true;
}

bool netconfig_reset(struct netconfig *netconfig)
{
    netconfig_ipv4_select_and_uninstall(netconfig);
    netconfig->rtm_protocol = 0;

    return true;
}

struct netconfig *netconfig_new(uint32_t ifindex)
{
    struct netconfig *netconfig;

    if (!netconfig_list)
        return NULL;

    l_debug("Starting netconfig for interface: %d", ifindex);

    netconfig = netconfig_find(ifindex);
    if (netconfig)
        return netconfig;

    netconfig = l_new(struct netconfig, 1);
    netconfig->ifindex = ifindex;
    netconfig->ifaddr_list = l_queue_new();

    l_queue_push_tail(netconfig_list, netconfig);

    return netconfig;
}

void netconfig_destroy(struct netconfig *netconfig)
{
    if (!netconfig_list)
        return;

    l_debug("");

    l_queue_remove(netconfig_list, netconfig);

    if (netconfig->rtm_protocol)
        netconfig_ipv4_select_and_uninstall(netconfig);

    netconfig_free(netconfig);
}

static int netconfig_init(void)
{
    uint32_t r;

    if (netconfig_list)
        return -EALREADY;

    rtnl = l_netlink_new(NETLINK_ROUTE);
    if (!rtnl) {
        l_error("netconfig: Failed to open route netlink socket");
        return -EPERM;
    }

    if (getenv("IWD_RTNL_DEBUG"))
        l_netlink_set_debug(rtnl, do_debug, "[NETCONFIG RTNL] ", NULL);

    r = l_netlink_register(rtnl, RTNLGRP_IPV4_IFADDR, netconfig_ifaddr_notify, NULL, NULL);
    if (!r) {
        l_error("netconfig: Failed to register for RTNL link address" " notifications.");
        goto error;
    }

    r = l_rtnl_ifaddr4_dump(rtnl, netconfig_ifaddr_cmd_cb, NULL, NULL);
    if (!r) {
        l_error("netconfig: Failed to get addresses from RTNL link.");
        goto error;
    }

    if (!l_settings_get_uint(iwd_get_config(), "Network", "RoutePriorityOffset", &ROUTE_PRIORITY_OFFSET))
        ROUTE_PRIORITY_OFFSET = 300;

    netconfig_list = l_queue_new();

    return 0;

 error:
    l_netlink_destroy(rtnl);
    rtnl = NULL;

    return r;
}

static void netconfig_exit(void)
{
    if (!netconfig_list)
        return;

    l_netlink_destroy(rtnl);
    rtnl = NULL;

    l_queue_destroy(netconfig_list, netconfig_free);
}

IWD_MODULE(netconfig, netconfig_init, netconfig_exit)
