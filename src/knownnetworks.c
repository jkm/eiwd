/*
 *
 *  Wireless daemon for Linux
 *
 *  Copyright (C) 2016-2019  Intel Corporation. All rights reserved.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <ell/ell.h>

#include "src/iwd.h"
#include "src/module.h"
#include "src/storage.h"
#include "src/common.h"
#include "src/network.h"
#include "src/knownnetworks.h"
#include "src/scan.h"
#include "src/util.h"
#include "src/watchlist.h"

static struct l_queue *known_networks;
static size_t num_known_hidden_networks;
static struct l_dir_watch *storage_dir_watch;
static struct watchlist known_network_watches;

static void network_info_free(void *data)
{
    struct network_info *network = data;

    network->ops->free(network);
}

static int connected_time_compare(const void *a, const void *b, void *user_data)
{
    const struct network_info *ni_a = a;
    const struct network_info *ni_b = b;

    if (l_time_after(ni_a->connected_time, ni_b->connected_time))
        return -1;
    else if (l_time_before(ni_a->connected_time, ni_b->connected_time))
        return 1;

    return 0;
}

/*
 * Finds the position n of this network_info in the list of known networks
 * sorted by connected_time.  E.g. an offset of 0 means the most recently
 * used network.  Only networks with seen_count > 0 are considered.  E.g.
 * only networks that appear in scan results on at least one wifi card.
 *
 * Returns -ENOENT if the entry couldn't be found.
 */
int known_network_offset(const struct network_info *target)
{
    const struct l_queue_entry *entry;
    const struct network_info *info;
    int n = 0;

    for (entry = l_queue_get_entries(known_networks); entry; entry = entry->next) {
        info = entry->data;
        if (target == info)
            return n;

        if (info->seen_count)
            n += 1;
    }

    return -ENOENT;
}

static int known_network_touch(struct network_info *info)
{
    return storage_network_touch(info->type, info->ssid);
}

static struct l_settings *known_network_open(struct network_info *info)
{
    return storage_network_open(info->type, info->ssid);
}

static void known_network_sync(struct network_info *info, struct l_settings *settings)
{
    storage_network_sync(info->type, info->ssid, settings);
}

static void known_network_remove(struct network_info *info)
{
    storage_network_remove(info->type, info->ssid);
}

static void known_network_free(struct network_info *info)
{
    l_free(info);
}

static const char *known_network_get_name(const struct network_info *info)
{
    return info->ssid;
}

static const char *known_network_get_type(const struct network_info *info)
{
    return security_to_str(info->type);
}

static const char *known_network_get_path(const struct network_info *info)
{
    return storage_get_network_file_path(info->type, info->ssid);
}

static struct network_info_ops known_network_ops = {
    .open = known_network_open,
    .touch = known_network_touch,
    .sync = known_network_sync,
    .remove = known_network_remove,
    .free = known_network_free,
    .get_path = known_network_get_path,
    .get_name = known_network_get_name,
    .get_type = known_network_get_type,
};

struct l_settings *network_info_open_settings(struct network_info *info)
{
    return info->ops->open(info);
}

int network_info_touch(struct network_info *info)
{
    return info->ops->touch(info);
}

const char *network_info_get_path(const struct network_info *info)
{
    return info->ops->get_path(info);
}

const char *network_info_get_name(const struct network_info *info)
{
    return info->ops->get_name(info);
}

const char *network_info_get_type(const struct network_info *info)
{
    return info->ops->get_type(info);
}

void known_network_set_connected_time(struct network_info *network, uint64_t connected_time)
{
    if (network->connected_time == connected_time)
        return;

    network->connected_time = connected_time;

    l_queue_remove(known_networks, network);
    l_queue_insert(known_networks, network, connected_time_compare, NULL);
}

void known_network_update(struct network_info *network, struct l_settings *settings)
{
    bool is_hidden;

    if (!l_settings_get_bool(settings, "Settings", "Hidden", &is_hidden))
        is_hidden = false;

    if (network->is_hidden != is_hidden) {
        if (network->is_hidden && !is_hidden)
            num_known_hidden_networks--;
        else if (!network->is_hidden && is_hidden)
            num_known_hidden_networks++;
    }

    network->is_hidden = is_hidden;
}

bool known_networks_foreach(known_networks_foreach_func_t function, void *user_data)
{
    const struct l_queue_entry *entry;

    for (entry = l_queue_get_entries(known_networks); entry; entry = entry->next)
        if (!function(entry->data, user_data))
            break;

    return !entry;
}

bool known_networks_has_hidden(void)
{
    return num_known_hidden_networks ? true : false;
}

static bool network_info_match(const void *a, const void *b)
{
    const struct network_info *ni_a = a;
    const struct network_info *ni_b = b;

    if (ni_a->type != ni_b->type)
        return false;

    if (strcmp(ni_a->ssid, ni_b->ssid))
        return false;

    return true;
}

struct network_info *known_networks_find(const char *ssid, enum security security)
{
    struct network_info query;

    query.type = security;
    strcpy(query.ssid, ssid);

    return l_queue_find(known_networks, network_info_match, &query);
}

void known_networks_remove(struct network_info *network)
{
    if (network->is_hidden)
        num_known_hidden_networks--;

    l_queue_remove(known_networks, network);

    WATCHLIST_NOTIFY(&known_network_watches, known_networks_watch_func_t, KNOWN_NETWORKS_EVENT_REMOVED, network);

    network_info_free(network);
}

void known_networks_add(struct network_info *network)
{
    l_queue_insert(known_networks, network, connected_time_compare, NULL);

    WATCHLIST_NOTIFY(&known_network_watches, known_networks_watch_func_t, KNOWN_NETWORKS_EVENT_ADDED, network);
}

static void known_network_new(const char *ssid, enum security security,
                              struct l_settings *settings, uint64_t connected_time)
{
    bool is_hidden;
    struct network_info *network;

    network = l_new(struct network_info, 1);
    strcpy(network->ssid, ssid);
    network->type = security;
    network->connected_time = connected_time;
    network->ops = &known_network_ops;

    if (!l_settings_get_bool(settings, "Settings", "Hidden", &is_hidden))
        is_hidden = false;

    if (is_hidden)
        num_known_hidden_networks++;

    network->is_hidden = is_hidden;

    known_networks_add(network);
}

static void known_networks_watch_cb(const char *filename, enum l_dir_watch_event event, void *user_data)
{
    const char *ssid;
    L_AUTO_FREE_VAR(char *, full_path) = NULL;
    enum security security;
    struct network_info *network_before;
    struct l_settings *settings;
    uint64_t connected_time;

    /*
     * Ignore notifications for the actual directory, we can't do
     * anything about some of them anyway.  Only react to
     * notifications for files in the storage directory.
     */
    if (!filename)
        return;

    ssid = storage_network_ssid_from_path(filename, &security);
    if (!ssid)
        return;

    network_before = known_networks_find(ssid, security);

    full_path = storage_get_network_file_path(security, ssid);

    switch (event) {
        case L_DIR_WATCH_EVENT_CREATED:
        case L_DIR_WATCH_EVENT_REMOVED:
        case L_DIR_WATCH_EVENT_MODIFIED:
            /*
             * For now treat all the operations the same.  E.g. they may
             * result in the removal of the network (file moved out, not
             * readable or invalid) or the creation of a new network (file
             * created, permissions granted, syntax fixed, etc.)
             * so we always need to re-read the file.
             */
            settings = storage_network_open(security, ssid);

            if (settings) {
                connected_time = l_path_get_mtime(full_path);

                if (network_before) {
                    known_network_set_connected_time(network_before, connected_time);
                    known_network_update(network_before, settings);
                } else
                    known_network_new(ssid, security, settings, connected_time);
            } else if (network_before)
                known_networks_remove(network_before);

            l_settings_free(settings);

            break;
        case L_DIR_WATCH_EVENT_ACCESSED:
            break;
        case L_DIR_WATCH_EVENT_ATTRIB:
            if (network_before) {
                connected_time = l_path_get_mtime(full_path);
                known_network_set_connected_time(network_before, connected_time);
            }

            break;
    }
}

static void known_networks_watch_destroy(void *user_data)
{
    storage_dir_watch = NULL;
}

uint32_t known_networks_watch_add(known_networks_watch_func_t func,
                                  void *user_data, known_networks_destroy_func_t destroy)
{
    return watchlist_add(&known_network_watches, func, user_data, destroy);
}

void known_networks_watch_remove(uint32_t id)
{
    watchlist_remove(&known_network_watches, id);
}

static int known_networks_init(void)
{
    DIR *dir;
    struct dirent *dirent;

    L_AUTO_FREE_VAR(char *, storage_dir) = storage_get_path(NULL);

    dir = opendir(storage_dir);
    if (!dir) {
        l_info("Unable to open %s: %s", storage_dir, strerror(errno));
        return -ENOENT;
    }

    known_networks = l_queue_new();

    while ((dirent = readdir(dir))) {
        const char *ssid;
        enum security security;
        struct l_settings *settings;
        uint64_t connected_time;
        L_AUTO_FREE_VAR(char *, full_path) = NULL;

        if (dirent->d_type != DT_REG && dirent->d_type != DT_LNK)
            continue;

        ssid = storage_network_ssid_from_path(dirent->d_name, &security);
        if (!ssid)
            continue;

        settings = storage_network_open(security, ssid);

        full_path = storage_get_network_file_path(security, ssid);

        if (settings) {
            connected_time = l_path_get_mtime(full_path);

            known_network_new(ssid, security, settings, connected_time);
        }

        l_settings_free(settings);
    }

    closedir(dir);

    storage_dir_watch = l_dir_watch_new(storage_dir, known_networks_watch_cb, NULL, known_networks_watch_destroy);
    watchlist_init(&known_network_watches, NULL);

    return 0;
}

static void known_networks_exit(void)
{
    l_dir_watch_destroy(storage_dir_watch);

    l_queue_destroy(known_networks, network_info_free);
    known_networks = NULL;

    watchlist_destroy(&known_network_watches);
}

IWD_MODULE(known_networks, known_networks_init, known_networks_exit)
