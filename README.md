# eiwd

eiwd is a fork of Intel's iwd, but with focus on simplicity
instead of adding numerous features.

### What distinguishes eiwd from the original iwd?

- no dbus dependency (just a single binary)
- no iwctl (just eiwd daemon)
- no IPv6 (just IPv4)
- no DHCP (just static configuration)
- no ANQP support
- no P2P support
- no Roaming support
- no TLS support


In short, this daemon just lets you connect to your WPA2-PSK
access point (router) and set a local IPv4 address. Nothing else.


#### Building

Building and installing eiwd is as simple as running `make` command:
```
$ git clone https://codeberg.org/jkm/eiwd.git
$ cd eiwd
$ make
$ make install
```

#### Configuring

In order to connect your device to existing Wi-Fi network,
you need to create `<your_wifi_ssid>.psk` file inside
`/var/lib/eiwd` directory with the following structure:
```
[Security]
Passphrase=<your_wifi_passphrase>

[IP]
Address=192.168.1.2
Gateway=192.168.1.1
Netmask=255.255.255.0
```

#### Running

Finally, run `eiwd` in the background with the following command:
```
$ eiwd &
```
