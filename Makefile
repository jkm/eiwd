CC = gcc
CFLAGS = -O2 -fsigned-char -fno-exceptions -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=2 -fPIC -fPIE -Wall

eiwd: libell.a
	$(CC) -c $(CFLAGS) src/*.c -I .
	$(CC) -static *.o libell.a -o $@
	rm -f *.o

libell.a:
	$(CC) -c $(CFLAGS) ell/*.c -I .
	rm -f $@
	ar cr $@ *.o
	ranlib $@
	rm -f *.o

clean:
	rm -f eiwd
	rm -f libell.a
	rm -f *.o

install:
	install -m 755 eiwd /usr/bin/eiwd
