/*
 *
 *  Wireless daemon for Linux
 *
 *  Copyright (C) 2018-2019  Intel Corporation. All rights reserved.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <linux/if_ether.h>

#include <ell/ell.h>

#include "src/util.h"
#include "src/iwd.h"
#include "src/module.h"
#include "src/common.h"
#include "src/watchlist.h"
#include "src/scan.h"
#include "src/netdev.h"
#include "src/wiphy.h"
#include "src/network.h"
#include "src/knownnetworks.h"
#include "src/ie.h"
#include "src/handshake.h"
#include "src/station.h"
#include "src/mpdu.h"
#include "src/netconfig.h"

static struct l_queue *station_list;
static uint32_t netdev_watch;
static uint32_t mfp_setting;

struct station {
    enum station_state state;
    struct watchlist state_watches;
    struct scan_bss *connected_bss;
    struct network *connected_network;
    struct scan_bss *connect_pending_bss;
    struct network *connect_pending_network;
    struct l_queue *autoconnect_list;
    struct l_queue *bss_list;
    struct l_queue *hidden_bss_list_sorted;
    struct l_hashmap *networks;
    struct l_queue *networks_sorted;
    struct signal_agent *signal_agent;
    uint32_t hidden_network_scan_id;

    struct wiphy *wiphy;
    struct netdev *netdev;

    struct netconfig *netconfig;

    bool scanning:1;
    bool autoconnect:1;
};

struct wiphy *station_get_wiphy(struct station *station)
{
    return station->wiphy;
}

struct netdev *station_get_netdev(struct station *station)
{
    return station->netdev;
}

struct network *station_get_connected_network(struct station *station)
{
    return station->connected_network;
}

bool station_is_busy(struct station *station)
{
    if (station->state != STATION_STATE_DISCONNECTED && station->state != STATION_STATE_AUTOCONNECT)
        return true;

    return false;
}

static bool station_is_autoconnecting(struct station *station)
{
    return station->state == STATION_STATE_AUTOCONNECT;
}

struct autoconnect_entry {
    uint16_t rank;
    struct network *network;
    struct scan_bss *bss;
};

static void station_property_set_scanning(struct station *station, bool scanning)
{
    if (station->scanning == scanning)
        return;

    station->scanning = scanning;
}

static void station_enter_state(struct station *station, enum station_state state);

static void station_autoconnect_next(struct station *station)
{
    struct autoconnect_entry *entry;
    int r;

    while ((entry = l_queue_pop_head(station->autoconnect_list))) {
        l_debug("Considering autoconnecting to BSS '%s' with SSID: %s,"
                " freq: %u, rank: %u, strength: %i",
                util_address_to_string(entry->bss->addr),
                network_get_ssid(entry->network), entry->bss->frequency, entry->rank, entry->bss->signal_strength);

        r = network_autoconnect(entry->network, entry->bss);
        l_free(entry);

        if (!r) {
            station_enter_state(station, STATION_STATE_CONNECTING);
            return;
        }
    }
}

static int autoconnect_rank_compare(const void *a, const void *b, void *user)
{
    const struct autoconnect_entry *new_ae = a;
    const struct autoconnect_entry *ae = b;

    return ae->rank - new_ae->rank;
}

static void station_add_autoconnect_bss(struct station *station, struct network *network, struct scan_bss *bss)
{
    double rankmod;
    struct autoconnect_entry *entry;

    /* See if network is autoconnectable (is a known network) */
    if (!network_rankmod(network, &rankmod))
        return;

    entry = l_new(struct autoconnect_entry, 1);
    entry->network = network;
    entry->bss = bss;
    entry->rank = bss->rank * rankmod;
    l_queue_insert(station->autoconnect_list, entry, autoconnect_rank_compare, NULL);
}

static void bss_free(void *data)
{
    struct scan_bss *bss = data;

    scan_bss_free(bss);
}

static bool process_network(const void *key, void *data, void *user_data)
{
    struct network *network = data;
    struct station *station = user_data;

    if (!network_bss_list_isempty(network)) {
        bool connected = network == station->connected_network;

        /* Build the network list ordered by rank */
        network_rank_update(network, connected);

        l_queue_insert(station->networks_sorted, network, network_rank_compare, NULL);

        return false;
    }

    /* Drop networks that have no more BSSs in range */
    l_debug("No remaining BSSs for SSID: %s -- Removing network", network_get_ssid(network));
    network_remove(network, -ERANGE);

    return true;
}

static const char *iwd_network_get_path(struct station *station, const char *ssid, enum security security)
{
    static char path[256];
    unsigned int pos, i;

    pos = snprintf(path, sizeof(path), "%s/", netdev_get_path(station->netdev));

    for (i = 0; ssid[i] && pos < sizeof(path); i++)
        pos += snprintf(path + pos, sizeof(path) - pos, "%02x", ssid[i]);

    snprintf(path + pos, sizeof(path) - pos, "_%s", security_to_str(security));

    return path;
}

struct network *station_network_find(struct station *station, const char *ssid, enum security security)
{
    const char *path = iwd_network_get_path(station, ssid, security);

    return l_hashmap_lookup(station->networks, path);
}

static int bss_signal_strength_compare(const void *a, const void *b, void *user)
{
    const struct scan_bss *new_bss = a;
    const struct scan_bss *bss = b;

    return bss->signal_strength - new_bss->signal_strength;
}

/*
 * Returns the network object the BSS was added to or NULL if ignored.
 */
static struct network *station_add_seen_bss(struct station *station, struct scan_bss *bss)
{
    struct network *network;
    struct ie_rsn_info info;
    int r;
    enum security security;
    const char *path;
    char ssid[33];

    l_debug("Processing BSS '%s' with SSID: %s, freq: %u, rank: %u, "
            "strength: %i",
            util_address_to_string(bss->addr),
            util_ssid_to_utf8(bss->ssid_len, bss->ssid), bss->frequency, bss->rank, bss->signal_strength);

    if (util_ssid_is_hidden(bss->ssid_len, bss->ssid)) {
        l_debug("BSS has hidden SSID");

        l_queue_insert(station->hidden_bss_list_sorted, bss, bss_signal_strength_compare, NULL);
        return NULL;
    }

    if (!util_ssid_is_utf8(bss->ssid_len, bss->ssid)) {
        l_debug("Ignoring BSS with non-UTF8 SSID");
        return NULL;
    }

    memcpy(ssid, bss->ssid, bss->ssid_len);
    ssid[bss->ssid_len] = '\0';

    if (!(bss->capability & IE_BSS_CAP_ESS)) {
        l_debug("Ignoring non-ESS BSS \"%s\"", ssid);
        return NULL;
    }

    memset(&info, 0, sizeof(info));
    r = scan_bss_get_rsn_info(bss, &info);
    if (r < 0) {
        if (r != -ENOENT)
            return NULL;

        security = security_determine(bss->capability, NULL);
    } else
        security = security_determine(bss->capability, &info);

    path = iwd_network_get_path(station, ssid, security);

    network = l_hashmap_lookup(station->networks, path);
    if (!network) {
        network = network_create(station, ssid, security);

        if (!network_register(network, path)) {
            network_remove(network, -EINVAL);
            return NULL;
        }

        l_hashmap_insert(station->networks, network_get_path(network), network);
        l_debug("Added new Network \"%s\" security %s", network_get_ssid(network), security_to_str(security));
    }

    network_bss_add(network, bss);

    return network;
}

static bool bss_match(const void *a, const void *b)
{
    const struct scan_bss *bss_a = a;
    const struct scan_bss *bss_b = b;

    return !memcmp(bss_a->addr, bss_b->addr, sizeof(bss_a->addr));
}

struct bss_expiration_data {
    struct scan_bss *connected_bss;
    uint64_t now;
};

#define SCAN_RESULT_BSS_RETENTION_TIME (30 * 1000000)

static bool bss_free_if_expired(void *data, void *user_data)
{
    struct scan_bss *bss = data;
    struct bss_expiration_data *expiration_data = user_data;

    if (bss == expiration_data->connected_bss)
        /* Do not expire the currently connected BSS. */
        return false;

    if (l_time_before(expiration_data->now, bss->time_stamp + SCAN_RESULT_BSS_RETENTION_TIME))
        return false;

    bss_free(bss);

    return true;
}

static void station_bss_list_remove_expired_bsses(struct station *station)
{
    struct bss_expiration_data data = {
        .now = l_time_now(),
        .connected_bss = station->connected_bss,
    };

    l_queue_foreach_remove(station->bss_list, bss_free_if_expired, &data);
}

struct nai_search {
    struct network *network;
    const char **realms;
};

static void network_add_foreach(struct network *network, void *user_data)
{
    struct station *station = user_data;
    struct scan_bss *bss = network_bss_select(network, false);

    if (!bss)
        return;

    station_add_autoconnect_bss(station, network, bss);
}

/*
 * Used when scan results were obtained; either from scan running
 * inside station module or scans running in other state machines, e.g. wsc
 */
void station_set_scan_results(struct station *station, struct l_queue *new_bss_list, bool add_to_autoconnect)
{
    const struct l_queue_entry *bss_entry;
    struct network *network;

    while ((network = l_queue_pop_head(station->networks_sorted)))
        network_bss_list_clear(network);

    l_queue_clear(station->hidden_bss_list_sorted, NULL);

    l_queue_destroy(station->autoconnect_list, l_free);
    station->autoconnect_list = l_queue_new();

    station_bss_list_remove_expired_bsses(station);

    for (bss_entry = l_queue_get_entries(station->bss_list); bss_entry; bss_entry = bss_entry->next) {
        struct scan_bss *old_bss = bss_entry->data;
        struct scan_bss *new_bss;

        new_bss = l_queue_find(new_bss_list, bss_match, old_bss);
        if (new_bss) {
            if (old_bss == station->connected_bss)
                station->connected_bss = new_bss;

            bss_free(old_bss);

            continue;
        }

        if (old_bss == station->connected_bss) {
            l_warn("Connected BSS not in scan results");
            station->connected_bss->rank = 0;
        }

        l_queue_push_tail(new_bss_list, old_bss);
    }

    l_queue_destroy(station->bss_list, NULL);

    for (bss_entry = l_queue_get_entries(new_bss_list); bss_entry; bss_entry = bss_entry->next) {
        struct scan_bss *bss = bss_entry->data;
        struct network *network = station_add_seen_bss(station, bss);

        if (!network)
            continue;
    }

    station->bss_list = new_bss_list;

    l_hashmap_foreach_remove(station->networks, process_network, station);

    if (add_to_autoconnect) {
        station_network_foreach(station, network_add_foreach, station);
        station_autoconnect_next(station);
    }
}

static void station_handshake_event(struct handshake_state *hs, enum handshake_event event, void *user_data, ...)
{
    struct station *station = user_data;
    struct network *network = station->connected_network;
    va_list args;

    va_start(args, user_data);

    switch (event) {
        case HANDSHAKE_EVENT_STARTED:
            l_debug("Handshaking");
            break;
        case HANDSHAKE_EVENT_SETTING_KEYS:
            l_debug("Setting keys");

            /* If we got here, then our PSK works.  Save if required */
            network_sync_psk(network);
            break;
        case HANDSHAKE_EVENT_FAILED:
            netdev_handshake_failed(hs, va_arg(args, int));
            break;
        case HANDSHAKE_EVENT_REKEY_FAILED:
        case HANDSHAKE_EVENT_COMPLETE:
        case HANDSHAKE_EVENT_SETTING_KEYS_FAILED:
        case HANDSHAKE_EVENT_EAP_NOTIFY:
            /*
             * currently we don't care about any other events. The
             * netdev_connect_cb will notify us when the connection is
             * complete.
             */
            break;
    }

    va_end(args);
}

static int station_build_handshake_rsn(struct handshake_state *hs,
                                       struct wiphy *wiphy, struct network *network, struct scan_bss *bss)
{
    enum security security = network_get_security(network);
    bool add_mde = false;
    bool fils_hint = false;

    struct ie_rsn_info bss_info;
    uint8_t rsne_buf[256];
    struct ie_rsn_info info;
    uint8_t *ap_ie;

    memset(&info, 0, sizeof(info));

    memset(&bss_info, 0, sizeof(bss_info));
    scan_bss_get_rsn_info(bss, &bss_info);

    info.akm_suites = wiphy_select_akm(wiphy, bss, fils_hint);

    /*
     * Special case for OWE. With OWE we still need to build up the
     * handshake object with AKM/cipher info since OWE does the full 4-way
     * handshake. But if this is a non-OWE open network, we can skip this.
     */
    if (security == SECURITY_NONE && !(info.akm_suites & IE_RSN_AKM_SUITE_OWE))
        goto open_network;

    if (!info.akm_suites)
        goto not_supported;

    info.pairwise_ciphers = wiphy_select_cipher(wiphy, bss_info.pairwise_ciphers);
    info.group_cipher = wiphy_select_cipher(wiphy, bss_info.group_cipher);

    if (!info.pairwise_ciphers || !info.group_cipher)
        goto not_supported;

    /* Management frame protection is explicitly off for OSEN */
    if (info.akm_suites & IE_RSN_AKM_SUITE_OSEN) {
        info.group_management_cipher = IE_RSN_CIPHER_SUITE_NO_GROUP_TRAFFIC;
        goto build_ie;
    }

    switch (mfp_setting) {
        case 0:
            break;
        case 1:
            info.group_management_cipher = wiphy_select_cipher(wiphy, bss_info.group_management_cipher);
            info.mfpc = info.group_management_cipher != 0;
            break;
        case 2:
            info.group_management_cipher = wiphy_select_cipher(wiphy, bss_info.group_management_cipher);

            /*
             * MFP required on our side, but AP doesn't support MFP
             * or cipher mismatch
             */
            if (info.group_management_cipher == 0)
                goto not_supported;

            info.mfpc = true;
            info.mfpr = true;
            break;
    }

    if (bss_info.mfpr && !info.mfpc)
        goto not_supported;

 build_ie:
    /* RSN takes priority */
    if (bss->rsne) {
        ap_ie = bss->rsne;
        ie_build_rsne(&info, rsne_buf);
    } else if (bss->wpa) {
        ap_ie = bss->wpa;
        ie_build_wpa(&info, rsne_buf);
    } else if (bss->osen) {
        ap_ie = bss->osen;
        ie_build_osen(&info, rsne_buf);
    } else
        goto not_supported;

    if (!handshake_state_set_authenticator_ie(hs, ap_ie))
        goto not_supported;

    if (!handshake_state_set_supplicant_ie(hs, rsne_buf))
        goto not_supported;

    if (info.akm_suites & (IE_RSN_AKM_SUITE_FT_OVER_8021X |
                           IE_RSN_AKM_SUITE_FT_USING_PSK |
                           IE_RSN_AKM_SUITE_FT_OVER_SAE_SHA256 |
                           IE_RSN_AKM_SUITE_FT_OVER_FILS_SHA256 | IE_RSN_AKM_SUITE_FT_OVER_FILS_SHA384))
        add_mde = true;

 open_network:
    if (security == SECURITY_NONE)
        /* Perform FT association if available */
        add_mde = bss->mde_present;

    if (add_mde) {
        uint8_t mde[5];

        /* The MDE advertised by the BSS must be passed verbatim */
        mde[0] = IE_TYPE_MOBILITY_DOMAIN;
        mde[1] = 3;
        memcpy(mde + 2, bss->mde, 3);

        handshake_state_set_mde(hs, mde);
    }

    return 0;

 not_supported:
    return -ENOTSUP;
}

static struct handshake_state *station_handshake_setup(struct station *station,
                                                       struct network *network, struct scan_bss *bss)
{
    enum security security = network_get_security(network);
    struct l_settings *settings = network_get_settings(network);
    struct wiphy *wiphy = station->wiphy;
    struct handshake_state *hs;
    const char *ssid;
    uint32_t eapol_proto_version;
    const char *value;
    bool full_random;
    bool override = false;
    uint8_t new_addr[ETH_ALEN];

    hs = netdev_handshake_state_new(station->netdev);

    handshake_state_set_event_func(hs, station_handshake_event, station);

    if (station_build_handshake_rsn(hs, wiphy, network, bss) < 0)
        goto not_supported;

    ssid = network_get_ssid(network);
    handshake_state_set_ssid(hs, (void *)ssid, strlen(ssid));

    if (settings && l_settings_get_uint(settings, "EAPoL", "ProtocolVersion", &eapol_proto_version)) {
        if (eapol_proto_version > 3) {
            l_warn("Invalid ProtocolVersion value - should be 0-3");
            eapol_proto_version = 0;
        }

        if (eapol_proto_version)
            l_debug("Overriding EAPoL protocol version to: %u", eapol_proto_version);

        handshake_state_set_protocol_version(hs, eapol_proto_version);
    }

    if (security == SECURITY_PSK) {
        /* SAE will generate/set the PMK */
        if (IE_AKM_IS_SAE(hs->akm_suite)) {
            const char *passphrase = network_get_passphrase(network);

            if (!passphrase)
                goto no_psk;

            handshake_state_set_passphrase(hs, passphrase);
        } else {
            const uint8_t *psk = network_get_psk(network);

            if (!psk)
                goto no_psk;

            handshake_state_set_pmk(hs, psk, 32);
        }
    } else if (security == SECURITY_8021X)
        handshake_state_set_8021x_config(hs, network_get_settings(network));

    /*
     * We have three possible options here:
     * 1. per-network MAC generation (default, no option in network config)
     * 2. per-network full MAC randomization
     * 3. per-network MAC override
     */

    if (!l_settings_get_bool(settings, "Settings", "AlwaysRandomizeAddress", &full_random))
        full_random = false;

    value = l_settings_get_value(settings, "Settings", "AddressOverride");
    if (value) {
        if (util_string_to_address(value, new_addr) && util_is_valid_sta_address(new_addr))
            override = true;
        else
            l_warn("[Network].AddressOverride is not a valid " "MAC address");
    }

    if (override && full_random) {
        l_warn("Cannot use both AlwaysRandomizeAddress and " "AddressOverride concurrently, defaulting to override");
        full_random = false;
    }

    if (override)
        handshake_state_set_supplicant_address(hs, new_addr);
    else if (full_random) {
        wiphy_generate_random_address(wiphy, new_addr);
        handshake_state_set_supplicant_address(hs, new_addr);
    }

    return hs;

 no_psk:
    l_warn("Missing network PSK/passphrase");
 not_supported:
    handshake_state_free(hs);

    return NULL;
}

static bool new_scan_results(int err, struct l_queue *bss_list, void *userdata)
{
    struct station *station = userdata;
    bool autoconnect;

    station_property_set_scanning(station, false);

    if (err)
        return false;

    autoconnect = station_is_autoconnecting(station);
    station_set_scan_results(station, bss_list, autoconnect);

    return true;
}

static void periodic_scan_trigger(int err, void *user_data)
{
    struct station *station = user_data;

    station_property_set_scanning(station, true);
}

static void periodic_scan_stop(struct station *station)
{
    uint64_t id = netdev_get_wdev_id(station->netdev);

    scan_periodic_stop(id);

    station_property_set_scanning(station, false);
}

static const char *station_state_to_string(enum station_state state)
{
    switch (state) {
        case STATION_STATE_DISCONNECTED:
            return "disconnected";
        case STATION_STATE_AUTOCONNECT:
            return "autoconnect";
        case STATION_STATE_CONNECTING:
            return "connecting";
        case STATION_STATE_CONNECTED:
            return "connected";
        case STATION_STATE_DISCONNECTING:
            return "disconnecting";
    }

    return "invalid";
}

static void station_enter_state(struct station *station, enum station_state state)
{
    uint64_t id = netdev_get_wdev_id(station->netdev);

    l_debug("Old State: %s, new state: %s", station_state_to_string(station->state), station_state_to_string(state));

    switch (state) {
        case STATION_STATE_AUTOCONNECT:
            scan_periodic_start(id, periodic_scan_trigger, new_scan_results, station);
            break;
        case STATION_STATE_CONNECTING:
        case STATION_STATE_DISCONNECTED:
            periodic_scan_stop(station);
            break;
        case STATION_STATE_CONNECTED:
            periodic_scan_stop(station);
            break;
        case STATION_STATE_DISCONNECTING:
            break;
    }

    station->state = state;

    WATCHLIST_NOTIFY(&station->state_watches, station_state_watch_func_t, state);
}

enum station_state station_get_state(struct station *station)
{
    return station->state;
}

uint32_t station_add_state_watch(struct station *station,
                                 station_state_watch_func_t func, void *user_data, station_destroy_func_t destroy)
{
    return watchlist_add(&station->state_watches, func, user_data, destroy);
}

bool station_remove_state_watch(struct station *station, uint32_t id)
{
    return watchlist_remove(&station->state_watches, id);
}

bool station_set_autoconnect(struct station *station, bool autoconnect)
{
    if (station->autoconnect == autoconnect)
        return true;

    station->autoconnect = autoconnect;

    if (station->state == STATION_STATE_DISCONNECTED && autoconnect)
        station_enter_state(station, STATION_STATE_AUTOCONNECT);

    if (station_is_autoconnecting(station) && !autoconnect)
        station_enter_state(station, STATION_STATE_DISCONNECTED);

    return true;
}

static void station_reset_connection_state(struct station *station)
{
    struct network *network = station->connected_network;

    if (!network)
        return;

    if (station->state == STATION_STATE_CONNECTED || station->state == STATION_STATE_CONNECTING)
        network_disconnected(network);

    station->connected_bss = NULL;
    station->connected_network = NULL;
}

static void station_disassociated(struct station *station)
{
    l_debug("%u", netdev_get_ifindex(station->netdev));

    if (station->netconfig)
        netconfig_reset(station->netconfig);

    station_reset_connection_state(station);

    station_enter_state(station, STATION_STATE_DISCONNECTED);

    if (station->autoconnect)
        station_enter_state(station, STATION_STATE_AUTOCONNECT);
}

static void station_connect_cb(struct netdev *netdev, enum netdev_result result, void *event_data, void *user_data);

static void station_disconnect_event(struct station *station, void *event_data)
{
    l_debug("%u", netdev_get_ifindex(station->netdev));

    station_disassociated(station);
}

static void station_netconfig_event_handler(enum netconfig_event event, void *user_data)
{
    struct station *station = user_data;

    switch (event) {
        case NETCONFIG_EVENT_CONNECTED:
            station_enter_state(station, STATION_STATE_CONNECTED);

            break;
        default:
            l_error("station: Unsupported netconfig event: %d.", event);
            break;
    }
}

static void station_netdev_event(struct netdev *netdev, enum netdev_event event, void *event_data, void *user_data);

static void station_lost_beacon(struct station *station)
{
    l_debug("%u", netdev_get_ifindex(station->netdev));

    return;
}

#define WNM_REQUEST_MODE_PREFERRED_CANDIDATE_LIST	(1 << 0)
#define WNM_REQUEST_MODE_TERMINATION_IMMINENT		(1 << 3)
#define WNM_REQUEST_MODE_ESS_DISASSOCIATION_IMMINENT	(1 << 4)

static void station_netdev_event(struct netdev *netdev, enum netdev_event event, void *event_data, void *user_data)
{
    struct station *station = user_data;

    switch (event) {
        case NETDEV_EVENT_AUTHENTICATING:
            l_debug("Authenticating");
            break;
        case NETDEV_EVENT_ASSOCIATING:
            l_debug("Associating");
            break;
        case NETDEV_EVENT_LOST_BEACON:
            station_lost_beacon(station);
            break;
        case NETDEV_EVENT_DISCONNECT_BY_AP:
        case NETDEV_EVENT_DISCONNECT_BY_SME:
            station_disconnect_event(station, event_data);
            break;
        case NETDEV_EVENT_RSSI_THRESHOLD_LOW:
            break;
        case NETDEV_EVENT_RSSI_THRESHOLD_HIGH:
            break;
        case NETDEV_EVENT_RSSI_LEVEL_NOTIFY:
            break;
    };
}

static bool station_try_next_bss(struct station *station)
{
    struct scan_bss *next;
    int ret;

    next = network_bss_select(station->connected_network, false);

    if (!next)
        return false;

    ret = station_connect_network(station, station->connected_network, next);
    if (ret < 0)
        return false;

    l_debug("Attempting to connect to next BSS " MAC, MAC_STR(next->addr));

    return true;
}

static bool station_retry_with_reason(struct station *station, uint16_t reason_code)
{
    /*
     * We don't want to cause a retry and blacklist if the password was
     * incorrect. Otherwise we would just continue to fail.
     *
     * Other reason codes can be added here if its decided we want to
     * fail in those cases.
     */
    if (reason_code == MMPDU_REASON_CODE_PREV_AUTH_NOT_VALID || reason_code == MMPDU_REASON_CODE_IEEE8021X_FAILED)
        return false;

    return station_try_next_bss(station);
}

/* A bit more consise for trying to fit these into 80 characters */
#define IS_TEMPORARY_STATUS(code) \
	((code) == MMPDU_STATUS_CODE_DENIED_UNSUFFICIENT_BANDWIDTH || \
	(code) == MMPDU_STATUS_CODE_DENIED_POOR_CHAN_CONDITIONS || \
	(code) == MMPDU_STATUS_CODE_REJECTED_WITH_SUGG_BSS_TRANS || \
	(code) == MMPDU_STATUS_CODE_DENIED_NO_MORE_STAS)

static bool station_retry_with_status(struct station *station, uint16_t status_code)
{
    /*
     * Certain Auth/Assoc failures should not cause a timeout blacklist.
     * In these cases we want to only temporarily blacklist the BSS until
     * the connection is complete.
     *
     * TODO: The WITH_SUGG_BSS_TRANS case should also include a neighbor
     *       report IE in the frame. This would allow us to target a
     *       specific BSS on our next attempt. There is currently no way to
     *       obtain that IE, but this should be done in the future.
     */
    if (IS_TEMPORARY_STATUS(status_code))
        network_blacklist_add(station->connected_network, station->connected_bss);

    return station_try_next_bss(station);
}

static void station_connect_cb(struct netdev *netdev, enum netdev_result result, void *event_data, void *user_data)
{
    struct station *station = user_data;

    l_debug("%u, result: %d", netdev_get_ifindex(station->netdev), result);

    switch (result) {
        case NETDEV_RESULT_OK:
            break;
        case NETDEV_RESULT_HANDSHAKE_FAILED:
            /* reason code in this case */
            if (station_retry_with_reason(station, l_get_u16(event_data)))
                return;

            break;
        case NETDEV_RESULT_AUTHENTICATION_FAILED:
        case NETDEV_RESULT_ASSOCIATION_FAILED:
            /* status code in this case */
            if (station_retry_with_status(station, l_get_u16(event_data)))
                return;

            break;
        default:
            break;
    }

    if (result != NETDEV_RESULT_OK) {
        if (result != NETDEV_RESULT_ABORTED) {
            network_connect_failed(station->connected_network);
            station_disassociated(station);
        }

        return;
    }

    network_connected(station->connected_network);

    if (station->netconfig)
        netconfig_configure(station->netconfig,
                            network_get_settings(station->connected_network),
                            netdev_get_address(station->netdev), station_netconfig_event_handler, station);
    else
        station_enter_state(station, STATION_STATE_CONNECTED);
}

int station_connect_network(struct station *station, struct network *network, struct scan_bss *bss)
{
    struct handshake_state *hs;
    int r;

    hs = station_handshake_setup(station, network, bss);
    if (!hs)
        return -ENOTSUP;

    r = netdev_connect(station->netdev, bss, hs, station_netdev_event, station_connect_cb, station);
    if (r < 0) {
        handshake_state_free(hs);
        return r;
    }

    station->connected_bss = bss;
    station->connected_network = network;

    return 0;
}

static void station_disconnect_cb(struct netdev *netdev, bool success, void *user_data)
{
    struct station *station = user_data;

    l_debug("%u, success: %d", netdev_get_ifindex(station->netdev), success);

    station_enter_state(station, STATION_STATE_DISCONNECTED);

    if (station->autoconnect)
        station_enter_state(station, STATION_STATE_AUTOCONNECT);
}

int station_disconnect(struct station *station)
{
    if (station->state == STATION_STATE_DISCONNECTING)
        return -EBUSY;

    if (!station->connected_bss)
        return -ENOTCONN;

    if (netdev_disconnect(station->netdev, station_disconnect_cb, station) < 0)
        return -EIO;

    if (station->netconfig)
        netconfig_reset(station->netconfig);

    /*
     * If the disconnect somehow fails we won't know if we're still
     * connected so we may as well indicate now that we're no longer
     * connected.
     */
    station_reset_connection_state(station);

    station_enter_state(station, STATION_STATE_DISCONNECTING);

    return 0;
}

struct signal_agent {
    char *owner;
    char *path;
    unsigned int disconnect_watch;
};

void station_foreach(station_foreach_func_t func, void *user_data)
{
    const struct l_queue_entry *entry;

    for (entry = l_queue_get_entries(station_list); entry; entry = entry->next) {
        struct station *station = entry->data;

        func(station, user_data);
    }
}

struct station *station_find(uint32_t ifindex)
{
    const struct l_queue_entry *entry;

    for (entry = l_queue_get_entries(station_list); entry; entry = entry->next) {
        struct station *station = entry->data;

        if (netdev_get_ifindex(station->netdev) == ifindex)
            return station;
    }

    return NULL;
}

struct network_foreach_data {
    station_network_foreach_func_t func;
    void *user_data;
};

static void network_foreach(const void *key, void *value, void *user_data)
{
    struct network_foreach_data *data = user_data;
    struct network *network = value;

    data->func(network, data->user_data);
}

void station_network_foreach(struct station *station, station_network_foreach_func_t func, void *user_data)
{
    struct network_foreach_data data = {
        .func = func,
        .user_data = user_data,
    };

    l_hashmap_foreach(station->networks, network_foreach, &data);
}

struct l_queue *station_get_bss_list(struct station *station)
{
    return station->bss_list;
}

struct scan_bss *station_get_connected_bss(struct station *station)
{
    return station->connected_bss;
}

static struct station *station_create(struct netdev *netdev)
{
    struct station *station;

    station = l_new(struct station, 1);
    watchlist_init(&station->state_watches, NULL);

    station->bss_list = l_queue_new();
    station->hidden_bss_list_sorted = l_queue_new();
    station->networks = l_hashmap_new();
    l_hashmap_set_hash_function(station->networks, l_str_hash);
    l_hashmap_set_compare_function(station->networks, (l_hashmap_compare_func_t) strcmp);
    station->networks_sorted = l_queue_new();

    station->wiphy = netdev_get_wiphy(netdev);
    station->netdev = netdev;

    l_queue_push_head(station_list, station);

    station_set_autoconnect(station, true);

    station->netconfig = netconfig_new(netdev_get_ifindex(netdev));

    return station;
}

static void station_netdev_watch(struct netdev *netdev, enum netdev_watch_event event, void *userdata)
{
    switch (event) {
        case NETDEV_WATCH_EVENT_UP:
        case NETDEV_WATCH_EVENT_NEW:
            if (netdev_get_iftype(netdev) == NETDEV_IFTYPE_STATION && netdev_get_is_up(netdev))
                station_create(netdev);
            break;
        case NETDEV_WATCH_EVENT_DOWN:
        case NETDEV_WATCH_EVENT_DEL:
            break;
        default:
            break;
    }
}

static int station_init(void)
{
    station_list = l_queue_new();
    netdev_watch = netdev_watch_add(station_netdev_watch, NULL, NULL);

    if (!l_settings_get_uint(iwd_get_config(), "General", "ManagementFrameProtection", &mfp_setting))
        mfp_setting = 1;

    if (mfp_setting > 2) {
        l_error("Invalid [General].ManagementFrameProtection value: %d," " using default of 1", mfp_setting);
        mfp_setting = 1;
    }

    return true;
}

static void station_exit(void)
{
    netdev_watch_remove(netdev_watch);
    l_queue_destroy(station_list, NULL);
    station_list = NULL;
}

IWD_MODULE(station, station_init, station_exit)
IWD_MODULE_DEPENDS(station, netconfig)
